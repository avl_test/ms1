.PHONY: all test build run push deploy

all: test  build run push deploy

test:
	@echo "Starting test"
	
build:
	./local/buildlocal.sh

run:
	./local/runlocal.sh

pretest:
	@echo "Starting pretest"

posttest:
	@echo "Starting posttest"

clean:
	@echo "cleaning containers"
